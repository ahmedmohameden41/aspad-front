import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { OrganigrammeService } from 'src/app/_services/organigramme.service';
import { PlanActionService } from 'src/app/_services/plan-action.service';

@Component({
  selector: 'app-equipes',
  templateUrl: './equipes.component.html',
  styleUrls: ['./equipes.component.css']
})
export class EquipesComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  idInstance:any= localStorage.getItem('InstanceActive');
  idObjectif:any= localStorage.getItem('ObjectifActive');
  idActivite:any= localStorage.getItem('ActiviteActive');
  entites:any;
  entitesActivite:any;
  collaborateursActivite:any;
  constructor(private router:Router,private planActionService:PlanActionService, private organigrammeService:OrganigrammeService, private app:AppComponent) { }

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.organigrammeService.getEntites(parseInt(this.idInstance))
    .subscribe(data=>{this.entites=data;},err=>{console.log(err);});

    this.planActionService.getEntitesActivite(parseInt(this.idActivite))
    .subscribe(data=>{this.entitesActivite=data;console.log(data);},err=>{console.log(err);});

    this.planActionService.getCollaborateursActivite(parseInt(this.idActivite))
    .subscribe(data=>{this.collaborateursActivite=data},err=>{console.log(err);});
  }

  onUpdateActivite(idEntite:number){
    this.planActionService.updateActivite({}, parseInt(this.idActivite), parseInt(this.idObjectif), idEntite).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
  onUpdateEntitesActivite(idEntite:number){
    let conf = confirm("Etes vous sure?");
    if(conf)
    this.planActionService.updateEntitesActivite({}, parseInt(this.idActivite), parseInt(this.idObjectif), idEntite).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
  onUpdateCollaborateurManager(idCollaborateur:number, manager:boolean){
    this.organigrammeService.updateCollaborateurManager(idCollaborateur, manager)
    .subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});
  }
  
  onCollaborateurActive(idCollaborateur:any){
    localStorage.setItem('CollaborateurActive',idCollaborateur);
  }
}
