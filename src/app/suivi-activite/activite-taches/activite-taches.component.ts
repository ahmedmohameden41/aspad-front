import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { OrganigrammeService } from 'src/app/_services/organigramme.service';
import { PlanActionService } from 'src/app/_services/plan-action.service';
import { StatistiquesService } from 'src/app/_services/statistiques.service';
import { SuiviActivitesService } from 'src/app/_services/suivi-activites.service';

@Component({
  selector: 'app-activite-taches',
  templateUrl: './activite-taches.component.html',
  styleUrls: ['./activite-taches.component.css']
})
export class ActiviteTachesComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  
  activiteTaches:any;
  tache:any;
  idActivite:any= localStorage.getItem('ActiviteActive');
  idInstance:any= localStorage.getItem('InstanceActive');
  idTache:any;
  collaborateursActivite:any;
  commentairesTache:any;
  idCollaborateur:any;
  tachesInstance:any;
  tachesInstanceNonCommence:any;
  tachesInstanceTermine:any;
  tachesInstanceAcheve:any;
  tachesInstanceEnCours:any;
  userActive:any;

  constructor(private statistiquesService:StatistiquesService, private planActionService:PlanActionService, private suiviActivitesService:SuiviActivitesService, private app:AppComponent, private router:Router) { }
  
  ngOnInit(): void {
      this.dtOptions = {
        responsive: false,
        lengthChange: false,
        autoWidth: false,
      };

  this.userActive=this.app.userActive;

    this.statistiquesService.getTachesByInstance(parseInt(this.idInstance))
    .subscribe(data=>{this.tachesInstance=data;},err=>{console.log(err);});

    this.statistiquesService.getTachesByInstanceNonCommence(parseInt(this.idInstance))
    .subscribe(data=>{this.tachesInstanceNonCommence=data;},err=>{console.log(err);});

    this.statistiquesService.getTachesByInstanceTermine(parseInt(this.idInstance))
    .subscribe(data=>{this.tachesInstanceTermine=data;},err=>{console.log(err);});

    this.statistiquesService.getTachesByInstanceAcheve(parseInt(this.idInstance))
    .subscribe(data=>{this.tachesInstanceAcheve=data;},err=>{console.log(err);});

    this.statistiquesService.getTachesByInstanceEnCours(parseInt(this.idInstance))
    .subscribe(data=>{this.tachesInstanceEnCours=data;},err=>{console.log(err);}); 

    this.suiviActivitesService.getTachesByActivite(parseInt(this.idActivite))
    .subscribe(data=>{this.activiteTaches=data;}, err=>{console.log(err)});  

    this.planActionService.getCollaborateursActivite(parseInt(this.idActivite))
    .subscribe(data=>{this.collaborateursActivite=data;},err=>{console.log(err);});
  }

  onTacheActivite(idTache:number){
    this.idTache=idTache;
    this.commentairesTache={};

    this.suiviActivitesService.getCommentairesTache(idTache)
    .subscribe(data=>{this.commentairesTache=data;}, err=>{console.log(err);});  
  }

  onAddNewCommentaireTache(commentaireTache:Object, idCollaborateur:number){
    this.suiviActivitesService.addNewCommentairesTache(commentaireTache, this.idTache, idCollaborateur)
    .subscribe(data=>{this.app.reloadCurrentRoute();}, err=>{console.log(err)});
  }

  onSelectedCollaborateur(idCollaborateur:number){
    this.idCollaborateur=idCollaborateur;
  }
  onAddNewTache(Tache:Object){
    console.log(Tache);
    this.suiviActivitesService.addNewTache(Tache, parseInt(this.idActivite), this.idCollaborateur).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeleteTache(idTache:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.suiviActivitesService.deleteTache(idTache).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetTache(idTache:number){
    this.tache={};
    this.suiviActivitesService.getTache(idTache).subscribe(data=>{this.tache=data;},err=>{console.log(err);});;
  }

  onUpdateTache(Tache:{}, idTache:number){
    this.suiviActivitesService.updateTache(Tache, idTache, parseInt(this.idActivite), this.userActive.idCollaborateur).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }



}
