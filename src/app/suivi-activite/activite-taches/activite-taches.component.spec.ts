import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiviteTachesComponent } from './activite-taches.component';

describe('ActiviteTachesComponent', () => {
  let component: ActiviteTachesComponent;
  let fixture: ComponentFixture<ActiviteTachesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ActiviteTachesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiviteTachesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
