import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { OrganigrammeService } from '../_services/organigramme.service';
import { StatistiquesService } from '../_services/statistiques.service';
import { SuiviActivitesService } from '../_services/suivi-activites.service';

@Component({
  selector: 'app-suivi-activite',
  templateUrl: './suivi-activite.component.html',
  styleUrls: ['./suivi-activite.component.css']
})
export class SuiviActiviteComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  
  mesTaches:any;
  tache:any;
  // idActivite:any= localStorage.getItem('ActiviteActive');
  idTache:any;  
  userActive:any=this.app.userActive;
  constructor(private suiviActivitesService:SuiviActivitesService,private statistiquesService:StatistiquesService, private app:AppComponent, private router:Router) { }

  ngOnInit(): void {
      this.dtOptions = {
        responsive: false,
        lengthChange: false,
        autoWidth: false,
      };
    
    this.suiviActivitesService.getTachesByUtilisateur(this.userActive.idCollaborateur)
    .subscribe(data=>{this.mesTaches=data;}, err=>{console.log(err)});  
  }

  onGetTache(idTache:number){
    this.tache={};
    this.suiviActivitesService.getTache(idTache).subscribe(data=>{this.tache=data;},err=>{console.log(err);});;
  }

  onUpdateTache(Tache:{}, idTache:number){
    this.suiviActivitesService.updateTache(Tache, idTache, this.userActive.activites.idActivite, this.userActive.idCollaborateur).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
}