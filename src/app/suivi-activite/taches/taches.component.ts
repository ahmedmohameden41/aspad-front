import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { SuiviActivitesService } from 'src/app/_services/suivi-activites.service';

@Component({
  selector: 'app-taches',
  templateUrl: './taches.component.html',
  styleUrls: ['./taches.component.css']
})
export class TachesComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  taches:any;
  tache:any;
  idCollaborateur:any= localStorage.getItem('CollaborateurActive');
  idActivite:any= localStorage.getItem('ActiviteActive');
  constructor(private router:Router, private suiviActivitesService:SuiviActivitesService, private app:AppComponent) { }

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.suiviActivitesService.getTaches(parseInt(this.idActivite), parseInt(this.idCollaborateur))
    .subscribe(data=>{this.taches=data;},err=>{console.log(err);});
  }

  onAddNewTache(Tache:Object){
    console.log(Tache);
    this.suiviActivitesService.addNewTache(Tache, parseInt(this.idActivite), parseInt(this.idCollaborateur)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeleteTache(idTache:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.suiviActivitesService.deleteTache(idTache).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetTache(idTache:number){
    this.tache={};
    this.suiviActivitesService.getTache(idTache).subscribe(data=>{this.tache=data;},err=>{console.log(err);});;
  }

  onUpdateTache(Tache:{}, idTache:number){
    this.suiviActivitesService.updateTache(Tache, idTache, parseInt(this.idActivite), parseInt(this.idCollaborateur)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

}
