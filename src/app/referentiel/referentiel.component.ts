import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { OrganigrammeService } from '../_services/organigramme.service';

@Component({
  selector: 'app-referentiel',
  templateUrl: './referentiel.component.html',
  styleUrls: ['./referentiel.component.css']
})
export class ReferentielComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  result:any;
  utilisateurs:any;
  utilisateur:any;
  selectedRole:any;
  roles = ['admin','gestionnaire','utilisateur','decideur'];

  constructor(private router:Router, private organigrammeService:OrganigrammeService, private app:AppComponent) { }

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.organigrammeService.getUtilisateurs()
      .subscribe(data=>{this.utilisateurs=data;},err=>{console.log(err);});
    // this.organigrammeService.getRoles()
    //   .subscribe(data=>{this.roles=data;},err=>{console.log(err);});
  }

  onAddUser(username:any, email:any, password:any){
    let User = {
      'username':username,
      'email':email,
      'password': password,
      'role': [this.selectedRole]
    }
    console.log(User);
    this.organigrammeService.addNewUser(User).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
  
  onSelectedRole(role:any){
    this.selectedRole=role;
  }

  onDeleteUtilisateur(username:String){
    let conf = confirm("Etes vous sure?");
    if(conf) this.organigrammeService.deleteUtilisateur(username).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetUtilisateur(username:String){
    this.utilisateur={};
    this.organigrammeService.getUtilisateur(username).subscribe(data=>{this.utilisateur=data;},err=>{console.log(err);});;
  }
}
