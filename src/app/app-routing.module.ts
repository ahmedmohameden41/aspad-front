import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './accueil/accueil.component';
import { LoginComponent } from './referentiel/login/login.component';
import { OrganigrammeComponent } from './organigramme/organigramme.component';
import { RegisterComponent } from './referentiel/register/register.component';
import { ProfileComponent } from './referentiel/profile/profile.component';
import { TableaudebordComponent } from './tableaudebord/tableaudebord.component';
import { EntitesComponent } from './organigramme/entites/entites.component';
import { CollaborateursComponent } from './organigramme/collaborateurs/collaborateurs.component';
import { FonctionsComponent } from './organigramme/fonctions/fonctions.component';
import { InstanceComponent } from './organigramme/instance/instance.component';
import { PlanActionComponent } from './plan-action/plan-action.component';
import { AxesComponent } from './plan-action/axes/axes.component';
import { ObjectifsComponent } from './plan-action/objectifs/objectifs.component';
import { ActivitesComponent } from './plan-action/activites/activites.component';
import { TachesComponent } from './suivi-activite/taches/taches.component';
import { ReferentielComponent } from './referentiel/referentiel.component';
import { SuiviActiviteComponent } from './suivi-activite/suivi-activite.component';
import { EquipesComponent } from './suivi-activite/equipes/equipes.component';
import { ActiviteTachesComponent } from './suivi-activite/activite-taches/activite-taches.component';

const routes: Routes = [
  { path:"accueil", component: AccueilComponent},
  { path:"organigramme", component: OrganigrammeComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'tableaudebord', component: TableaudebordComponent },
  { path: 'entites', component: EntitesComponent },
  { path: 'collaborateurs', component: CollaborateursComponent },
  { path: 'fonctions', component: FonctionsComponent },
  { path: 'instance', component: InstanceComponent },
  { path: 'plandaction', component: PlanActionComponent },
  { path: 'axes', component: AxesComponent },
  { path: 'objectifs', component: ObjectifsComponent },
  { path: 'activites', component: ActivitesComponent },
  { path: 'taches', component: TachesComponent },
  { path: 'referentiel', component: ReferentielComponent },
  { path: 'mestaches', component: SuiviActiviteComponent },
  { path: 'activiteTaches', component: ActiviteTachesComponent },
  { path: 'equipes', component: EquipesComponent },
  { path:"", redirectTo:"tableaudebord", pathMatch:'full'}

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
