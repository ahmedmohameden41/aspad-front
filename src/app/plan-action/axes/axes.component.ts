import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { PlanActionService } from 'src/app/_services/plan-action.service';

@Component({
  selector: 'app-axes',
  templateUrl: './axes.component.html',
  styleUrls: ['./axes.component.css']
})
export class AxesComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  result:any;
  axes:any;
  axe:any;

  idPlanAction:any= localStorage.getItem('PlanActionActive');
  constructor(private router:Router, private planActionService:PlanActionService, private app:AppComponent) { }
  

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.planActionService.getAxes(parseInt(this.idPlanAction))
      .subscribe(data=>{this.axes=data;},err=>{console.log(err);});
  }
  onAddNewAxe(Axe:Object){
    console.log(Axe);
    this.planActionService.addNewAxe(Axe, parseInt(this.idPlanAction)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeleteAxe(idAxe:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.planActionService.deleteAxe(idAxe).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetAxe(idAxe:number){
    this.axe={};
    this.planActionService.getAxe(idAxe).subscribe(data=>{this.axe=data;},err=>{console.log(err);});;
  }

  onUpdateAxe(Axe:{}, idAxe:number){
    this.planActionService.updateAxe(Axe, idAxe, parseInt(this.idPlanAction)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onAxeActive(idAxe:any){
    localStorage.setItem('AxeActive',idAxe);
  }
}
