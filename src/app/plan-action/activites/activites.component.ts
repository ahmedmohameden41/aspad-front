import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { PlanActionService } from 'src/app/_services/plan-action.service';

@Component({
  selector: 'app-activites',
  templateUrl: './activites.component.html',
  styleUrls: ['./activites.component.css']
})
export class ActivitesComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  result:any;
  activites:any;
  activite:any;
  idObjectif:any= localStorage.getItem('ObjectifActive');
  constructor(private router:Router, private planActionService:PlanActionService, private app:AppComponent) { }

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.planActionService.getActivites(parseInt(this.idObjectif))
      .subscribe(data=>{this.activites=data;},err=>{console.log(err);});     
  }
  onAddNewActivite(Activite:Object){
    console.log(Activite);
    this.planActionService.addNewActivite(Activite, parseInt(this.idObjectif)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeleteActivite(idActivite:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.planActionService.deleteActivite(idActivite).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetActivite(idActivite:number){
    this.activite={};
    this.planActionService.getActivite(idActivite).subscribe(data=>{this.activite=data;},err=>{console.log(err);});;
  }

  onUpdateActivite(Activite:{}, idActivite:number){
        this.planActionService.updateActivite(Activite, idActivite, parseInt(this.idObjectif), 0).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
  onActiviteActive(idActivite:any){
    localStorage.setItem('ActiviteActive',idActivite);
  }
}

