import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { InstanceComponent } from '../organigramme/instance/instance.component';
import { PlanActionService } from '../_services/plan-action.service';

@Component({
  selector: 'app-plan-action',
  templateUrl: './plan-action.component.html',
  styleUrls: ['./plan-action.component.css']
})
export class PlanActionComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  result:any;
  planActions:any;
  PlanAction:any;
  idInstance:any= localStorage.getItem('InstanceActive');

  constructor(private router:Router, private planActionService:PlanActionService, private app:AppComponent) { }

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };        

    this.planActionService.getPlanActions(parseInt(this.idInstance))
      .subscribe(data=>{this.planActions=data;},err=>{console.log(err);});    
  }
  onAddNewPlanAction(PlanAction:Object){
    this.planActionService.addNewPlanAction(PlanAction, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeletePlanAction(idPlanAction:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.planActionService.deletePlanAction(idPlanAction).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetPlanAction(idPlanAction:number){
    this.PlanAction={};
    this.planActionService.getPlanAction(idPlanAction).subscribe(data=>{this.PlanAction=data;},err=>{console.log(err);});;
  }

  onUpdatePlanAction(PlanAction:{}, idPlanAction:number){
    console.log(PlanAction);
    this.planActionService.updatePlanAction(PlanAction, idPlanAction, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
  
  onPlanActionActive(idPlanAction:any){
    localStorage.setItem('PlanActionActive',idPlanAction);
  }
}
