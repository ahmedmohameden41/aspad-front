import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { PlanActionService } from 'src/app/_services/plan-action.service';

@Component({
  selector: 'app-objectifs',
  templateUrl: './objectifs.component.html',
  styleUrls: ['./objectifs.component.css']
})
export class ObjectifsComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  result:any;
  objectifs:any;
  objectif:any;
  idAxe:any= localStorage.getItem('AxeActive');

  constructor(private router:Router, private planActionService:PlanActionService, private app:AppComponent) { }

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.planActionService.getObjectifs(parseInt(this.idAxe))
      .subscribe(data=>{this.objectifs=data;},err=>{console.log(err);});
  }
  onAddNewObjectif(Objectif:Object){
    console.log(Objectif);
    this.planActionService.addNewObjectif(Objectif,  parseInt(this.idAxe)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeleteObjectif(idObjectif:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.planActionService.deleteObjectif(idObjectif).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetObjectif(idObjectif:number){
    this.objectif={};
    this.planActionService.getObjectif(idObjectif).subscribe(data=>{this.objectif=data;},err=>{console.log(err);});;
  }

  onUpdateObjectif(Objectif:{}, idObjectif:number){    
    this.planActionService.updateObjectif(Objectif, idObjectif, parseInt(this.idAxe)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
  
  onObjectifActive(idObjectif:any){
    localStorage.setItem('ObjectifActive',idObjectif);
  }
}
