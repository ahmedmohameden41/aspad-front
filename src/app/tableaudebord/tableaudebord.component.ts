import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { OrganigrammeService } from '../_services/organigramme.service';
import { PlanActionService } from '../_services/plan-action.service';
import { StatistiquesService } from '../_services/statistiques.service';
import { SuiviActivitesService } from '../_services/suivi-activites.service';

@Component({
  selector: 'app-tableaudebord',
  templateUrl: './tableaudebord.component.html',
  styleUrls: ['./tableaudebord.component.css']
})
export class TableaudebordComponent implements OnInit {
  dtOptions: DataTables.Settings = {};

  activites:any;
  entites:any;
  collaborateursInstance:any = [];
  activitesInstance:any = [];
  activitesInstanceEnCours:any = [];
  activitesInstanceNonCommence:any = [];
  activitesInstanceTermine:any = [];
  activitesInstanceAcheve:any = [];
  tachesInstance:any = [];
  idInstance:any= localStorage.getItem('InstanceActive');
  idActivite:any;
  userActive:any;
  commentairesActivite:any;
  constructor(private suiviActivitesService:SuiviActivitesService, private organigrammeService:OrganigrammeService, private router:Router, private statistiquesService:StatistiquesService, private app:AppComponent) { }

  ngOnInit(): void {
      this.dtOptions = {
        responsive: false,
        lengthChange: false,
        autoWidth: false,
      };

  this.userActive=this.app.userActive;

    this.organigrammeService.getEntites(parseInt(this.idInstance))
    .subscribe(data=>{this.entites=data;},err=>{console.log(err);});

    this.statistiquesService.getCollaborateursByInstance(parseInt(this.idInstance))
    .subscribe(data=>{this.collaborateursInstance=data;},err=>{console.log(err);});

    this.statistiquesService.getActivitesByInstance(parseInt(this.idInstance))
    .subscribe(data=>{this.activitesInstance=data;},err=>{console.log(err);});

    this.statistiquesService.getTachesByInstance(parseInt(this.idInstance))
    .subscribe(data=>{this.tachesInstance=data;},err=>{console.log(err);});

    this.statistiquesService.getActivitesByInstanceNonCommence(parseInt(this.idInstance))
    .subscribe(data=>{this.activitesInstanceNonCommence=data;},err=>{console.log(err);});

    this.statistiquesService.getActivitesByInstanceTermine(parseInt(this.idInstance))
    .subscribe(data=>{this.activitesInstanceTermine=data;},err=>{console.log(err);});

    this.statistiquesService.getActivitesByInstanceAcheve(parseInt(this.idInstance))
    .subscribe(data=>{this.activitesInstanceAcheve=data;},err=>{console.log(err);});

    this.statistiquesService.getActivitesByInstanceEnCours(parseInt(this.idInstance))
    .subscribe(data=>{this.activitesInstanceEnCours=data;},err=>{console.log(err);});
  }

  onActiviteActivite(idActivite:number){
    this.idActivite=idActivite;
    this.commentairesActivite={};

    this.suiviActivitesService.getCommentairesActivite(idActivite)
    .subscribe(data=>{this.commentairesActivite=data;},err=>{console.log(err);});
  }

  onAddNewCommentaireActivite(commentaireActivite:Object, idCollaborateur:number){
    this.suiviActivitesService.addNewCommentairesActivite(commentaireActivite, this.idActivite, idCollaborateur)
    .subscribe(data=>{this.app.reloadCurrentRoute();}, err=>{console.log(err)});
  }

  activiteActivite(idActivite:any){
    localStorage.setItem('ActiviteActive', idActivite);
  }
}
