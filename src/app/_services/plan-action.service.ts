import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const URL: String = 'http://localhost:8080/';

@Injectable({
  providedIn: 'root'
})

export class PlanActionService {

  constructor(private httpClient: HttpClient) { }


  public getPlanActions(idInstance: number): Observable<any> {
    return this.httpClient.get(URL + "instanceses/" + idInstance + "/planActions");
  }
  public getPlanAction(idPlanAction: number): Observable<any> {
    return this.httpClient.get(URL + "planActionses/" + idPlanAction);
  }
  public addNewPlanAction(PlanAction: Object, idInstance: number): Observable<any> {
    return this.httpClient.post(URL + "instanceses/" + idInstance + "/planActions", PlanAction);
  }
  public updatePlanAction(PlanAction: Object, idPlanAction: number, idInstance: number): Observable<any> {
    return this.httpClient.put(URL + "planActionses/" + idPlanAction + "/" + idInstance, PlanAction,);
  }
  public deletePlanAction(idPlanAction: number): Observable<any> {
    return this.httpClient.delete(URL + "planActionses/" + idPlanAction);
  }

  public getAxes(idPlanAction: number): Observable<any> {
    return this.httpClient.get(URL + "planActionses/" + idPlanAction + "/axes");
  }
  public getAxe(idAxe: number): Observable<any> {
    return this.httpClient.get(URL + "axeses/" + idAxe);
  }
  public addNewAxe(Axe: Object, idPlanAction: number): Observable<any> {
    return this.httpClient.post(URL + "planActionses/" + idPlanAction + "/axes", Axe);
  }
  public updateAxe(Axe: Object, idAxe: number, idPlanAction: number): Observable<any> {
    return this.httpClient.put(URL + "axeses/" + idAxe + "/" + idPlanAction, Axe);
  }
  public deleteAxe(idAxe: number): Observable<any> {
    return this.httpClient.delete(URL + "axeses/" + idAxe);
  }


  public addNewObjectif(idObjectif: Object, idAxe: number): Observable<any> {
    return this.httpClient.post(URL + "axeses/" + idAxe + "/objectifs", idObjectif);
  }
  public getObjectif(idObjectif: number): Observable<any> {
    return this.httpClient.get(URL + "objectifses/" + idObjectif);
  }

  public getObjectifs(idAxe: number): Observable<any> {
    return this.httpClient.get(URL + "axeses/" + idAxe + "/objectifs");
  }
  public updateObjectif(Objectif: Object, idObjectif: number, idAxe: number): Observable<any> {
    return this.httpClient.put(URL + "objectifses/" + idObjectif + "/" + idAxe, Objectif);
  }
  public deleteObjectif(idObjectif: number): Observable<any> {
    return this.httpClient.delete(URL + "objectifses/" + idObjectif);
  }


  public addNewActivite(idActivite: Object, idObjectif: number): Observable<any> {
    return this.httpClient.post(URL + "objectifses/" + idObjectif + "/activites", idActivite);
  }
  public getActivite(idActivite: number): Observable<any> {
    return this.httpClient.get(URL + "activiteses/" + idActivite);
  }
  public getActivites(idObjectif: number): Observable<any> {
    return this.httpClient.get(URL + "objectifses/" + idObjectif + "/activites");
  }
  public updateActivite(Activite: Object, idActivite: number, idObjectif: number, idEntite: number): Observable<any> {
    return this.httpClient.put(URL + "activiteses/" + idActivite + "/" + idObjectif + "/" + idEntite, Activite);
  }
  public deleteActivite(idActivite: number): Observable<any> {
    return this.httpClient.delete(URL + "activiteses/" + idActivite);
  }

  public getEntitesActivite(idActivite: number): Observable<any> {
    return this.httpClient.get(URL + "activiteses/" + idActivite + "/entites");
  }
  public getCollaborateursActivite(idActivite: number): Observable<any> {
    return this.httpClient.get(URL + "activiteses/" + idActivite + "/collaborateurs");
  }
  public updateEntitesActivite(Activite: Object, idActivite: number, idObjectif: number, idEntite: number): Observable<any> {
    return this.httpClient.put(URL + "activiteses/" + idActivite + "/" + idObjectif + "/" + idEntite+"/entites", Activite);
  }
}
