import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const  URL:String ='http://localhost:8080/';
const AUTH_API = 'http://localhost:8080/api/auth/';

@Injectable({
  providedIn: 'root'
})

export class OrganigrammeService {

  constructor(private httpClient:HttpClient) { }

  public getInstances(): Observable<any>{
    return this.httpClient.get(URL+"instanceses");
  }
  public getInstance(idInstance:number): Observable<any>{
    return this.httpClient.get(URL+"instanceses/"+idInstance);
  }
  public addNewInstance(Instance:Object): Observable<any>{
    return this.httpClient.post(URL+"instanceses", Instance);
  }
  public updateInstance(Instance:Object, idInstance:number): Observable<any>{
    return this.httpClient.put(URL+"instanceses/"+idInstance, Instance);
  }
  public deleteInstance(idInstance:number): Observable<any>{
    return this.httpClient.delete(URL+"instanceses/"+idInstance);
  }

  public getEntites(idInstance:number): Observable<any>{
    return this.httpClient.get(URL+"instanceses/"+idInstance+"/entites");
  }
  public getEntite(idEntite:number): Observable<any>{
    return this.httpClient.get(URL+"entiteses/"+idEntite);
  }
  public addNewEntite(Entite:Object, idInstance:number): Observable<any>{
    return this.httpClient.post(URL+"instanceses/"+idInstance+"/entites", Entite);
  }
  public updateEntite(Entite:Object, idEntite:number, idInstance:number): Observable<any>{
    return this.httpClient.put(URL+"entiteses/"+idEntite+"/"+idInstance, Entite);
  }
  public deleteEntite(idEntite:number): Observable<any>{
    return this.httpClient.delete(URL+"entiteses/"+idEntite);
  }


  public addNewCollaborateur(Collaborateur:Object, idEntite:any, idFonction:any): Observable<any>{
    return this.httpClient.post(URL+"entiteses/"+idEntite+"/collaborateurs/"+idFonction, Collaborateur);
  }
  public getCollaborateur(idCollaborateur:number): Observable<any>{
    return this.httpClient.get(URL+"collaborateurses/"+idCollaborateur);
  }
  public getCollaborateursByEmail(email:String): Observable<any>{
    return this.httpClient.get(URL+"collaborateurses/"+email+"/instance");
  }
  public getCollaborateurFonction(idCollaborateur:number): Observable<any>{
    return this.httpClient.get(URL+"collaborateurses/"+idCollaborateur+"/fonctions");
  }
  public getCollaborateurs(idEntite:number): Observable<any>{
    return this.httpClient.get(URL+"entiteses/"+idEntite+"/collaborateurs");
  }
  public updateCollaborateur(Collaborateur:Object, idCollaborateur:number, idEntite:number, idFonction:Number): Observable<any>{    
    return this.httpClient.put(URL+"collaborateurses/"+idCollaborateur+"/"+idEntite+"/"+idFonction, Collaborateur);
  }
  public updateCollaborateurManager(idCollaborateur:number, manager:boolean){
    return this.httpClient.put(URL+"collaborateurses/"+idCollaborateur+"/"+manager+"/equipes", {});
  }
  public deleteCollaborateur(idCollaborateur:number): Observable<any>{
    return this.httpClient.delete(URL+"collaborateurses/"+idCollaborateur);
  }


  public addNewFonction(idFonction:Object): Observable<any>{
    return this.httpClient.post(URL+"fonctionses", idFonction);
  }
  public getFonction(idFonction:number): Observable<any>{
    return this.httpClient.get(URL+"fonctionses/"+idFonction);
  }

  public getFonctions(): Observable<any>{
    return this.httpClient.get(URL+"fonctionses");
  }
  public updateFonction(Fonction:Object, idFonction:number): Observable<any>{    
    return this.httpClient.put(URL+"fonctionses/"+idFonction, Fonction);
  }
  public deleteFonction(idFonction:number): Observable<any>{
    return this.httpClient.delete(URL+"fonctionses/"+idFonction);
  }

  public addNewUser(user:Object): Observable<any>{
    return this.httpClient.post(AUTH_API+'signup', user);
  }
  public getRoles(): Observable<any>{
    return this.httpClient.get(URL+"roles");
  }
  public updateUser(user:Object, username:String): Observable<any> {
    return this.httpClient.put(AUTH_API+'users/'+username, user);
  }
  public getUtilisateurs(): Observable<any>{
    return this.httpClient.get(URL+"users");
  }
  public getUtilisateur(username:String): Observable<any>{
    return this.httpClient.get(AUTH_API+"users/"+username);
  }
  public deleteUtilisateur(username:String): Observable<any>{
    return this.httpClient.delete(AUTH_API+"users/"+username);
  }
}
