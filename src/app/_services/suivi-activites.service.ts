import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const  URL:String ='http://localhost:8080/';

@Injectable({
  providedIn: 'root'
})
export class SuiviActivitesService {

  constructor(private httpClient:HttpClient) { }

  public getTaches(idActivite:number, idCollaborateur:number): Observable<any>{
    return this.httpClient.get(URL+"tacheses/"+idActivite+"/"+idCollaborateur);
  }
  public getTache(idTache:number): Observable<any>{
    return this.httpClient.get(URL+"tacheses/"+idTache);
  }
  public addNewTache(Tache:Object, idActivite:number, idCollaborateur:number): Observable<any>{
    return this.httpClient.post(URL+"tacheses/"+idActivite+"/"+idCollaborateur, Tache);
  }
  public updateTache(Tache:Object, idTache:number, idActivite:number, idCollaborateur:number): Observable<any>{
    return this.httpClient.put(URL+"tacheses/"+idTache+"/"+idActivite+"/"+idCollaborateur, Tache);
  }
  public deleteTache(idTache:number): Observable<any>{
    return this.httpClient.delete(URL+"tacheses/"+idTache);
  }

  getTachesByUtilisateur(idCollaborateur:number): Observable<any>{
    return this.httpClient.get(URL+"collaborateurses/"+idCollaborateur+"/taches");
  }

  public getTachesByActivite(idActivite:number): Observable<any>{
    return this.httpClient.get(URL+"activiteses/"+idActivite+"/taches");
  }

  public getCommentairesActivite(idActivite:number): Observable<any>{
    return this.httpClient.get(URL+"activiteses/"+idActivite+"/commentairesActivites");
  } 
   
  public addNewCommentairesActivite(commentaireActivite:any, idActivite:number, idCollaborateur:number){
    return this.httpClient.post(URL+"commentairesActivites/"+idActivite+"/"+idCollaborateur, commentaireActivite);
  }

  public getCommentairesTache(idTache:number): Observable<any>{
    return this.httpClient.get(URL+"tacheses/"+idTache+"/commentairesTaches");
  } 
  public addNewCommentairesTache(commentaireTache:any, idTache:number, idCollaborateur:number){
    return this.httpClient.post(URL+"commentairesTaches/"+idTache+"/"+idCollaborateur, commentaireTache);
  }
}
