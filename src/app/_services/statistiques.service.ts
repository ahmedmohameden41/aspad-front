import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


const  URL:String ='http://localhost:8080/statistiques/';

@Injectable({
  providedIn: 'root'
})
export class StatistiquesService {
  
  constructor(private httpClient:HttpClient) { }

  public getCollaborateursByInstance(idInstance:number): Observable<any>{
    return this.httpClient.get(URL+"collaborateurs/"+idInstance);
  }

  public getTachesByInstance(idInstance:number): Observable<any>{
    return this.httpClient.get(URL+"taches/"+idInstance);
  }

  public getTachesByInstanceTermine(idInstance:number): Observable<any>{
    let statut:String ="Termine";
    return this.httpClient.get(URL+"taches/"+idInstance+"/"+statut);
  }

  public getTachesByInstanceNonCommence(idInstance:number): Observable<any>{
    let statut:String ="Non Commense";
    return this.httpClient.get(URL+"taches/"+idInstance+"/"+statut);
  }

  public getTachesByInstanceAcheve(idInstance:number): Observable<any>{
    let statut:String ="Acheve";
    return this.httpClient.get(URL+"taches/"+idInstance+"/"+statut);
  }

  public getTachesByInstanceEnCours(idInstance:number): Observable<any>{
    let statut:String ="En Cours";
    return this.httpClient.get(URL+"taches/"+idInstance+"/"+statut);
  }


  public getActivitesByInstance(idInstance:number): Observable<any>{
    return this.httpClient.get(URL+"activites/"+idInstance);
  }

  public getActivitesByInstanceTermine(idInstance:number): Observable<any>{
    let statut:String ="Termine";
    return this.httpClient.get(URL+"activites/"+idInstance+"/"+statut);
  }
  
  public getActivitesByInstanceNonCommence(idInstance:number): Observable<any>{
    let statut:String ="Non Commense";
    return this.httpClient.get(URL+"activites/"+idInstance+"/"+statut);
  }

  public getActivitesByInstanceAcheve(idInstance:number): Observable<any>{
    let statut:String ="Acheve";
    return this.httpClient.get(URL+"activites/"+idInstance+"/"+statut);
  }

  public getActivitesByInstanceEnCours(idInstance:number): Observable<any>{
    let statut:String ="En Cours";
    return this.httpClient.get(URL+"activites/"+idInstance+"/"+statut);
  }
}
