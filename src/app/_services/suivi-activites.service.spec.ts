import { TestBed } from '@angular/core/testing';

import { SuiviActivitesService } from './suivi-activites.service';

describe('SuiviActivitesService', () => {
  let service: SuiviActivitesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SuiviActivitesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
