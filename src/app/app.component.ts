import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { OrganigrammeService } from './_services/organigramme.service';
import { TokenStorageService } from './_services/token-storage.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  lang:any;
  title = 'ASPAD';
  roles: string[] = [];
  isLoggedIn = false;
  username?: string;
  email?: String;
  userActive:any;
  
  constructor(private organigrammeService:OrganigrammeService, private tokenStorageService: TokenStorageService, private translate: TranslateService, private router:Router) {
    this.translate.setDefaultLang('fr');
    this.translate.use(localStorage.getItem('lang') || 'fr');
  }

  ngOnInit(): void {
    this.lang = localStorage.getItem('lang');
    this.isLoggedIn = !!this.tokenStorageService.getToken();

    if (this.isLoggedIn) {
      const user = this.tokenStorageService.getUser();
      this.roles = user.roles;     
      this.username = user.username;
      this.email = user.email;

      this.organigrammeService.getCollaborateursByEmail(this.email || "")
    .subscribe(data=>{this.userActive=data;}, err=>{console.log(err)});
    }

    
  }
  logout(): void {
    this.tokenStorageService.signOut();
    window.location.reload();
  }

  useLanguage(value: any) {
     this.lang=value;
     localStorage.setItem('lang', this.lang);
     window.location.reload();
  }
  onRedirect(){
    this.router.navigate(['/accueil']);
  }

  reloadCurrentRoute(){
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(()=>{
      this.router.navigate([currentUrl]);
    });
  }
}
