import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { TokenStorageService } from '../_services/token-storage.service';

@Component({
  selector: 'app-asidenav',
  templateUrl: './asidenav.component.html',
  styles: [
  ]
})
export class AsidenavComponent implements OnInit {
  lang:any;
  currentUser: any;
  userActive:any;

  constructor(private router: Router, private app:AppComponent, private token: TokenStorageService) { }

  ngOnInit(): void {
    this.lang = localStorage.getItem('lang') || 'fr';
    this.currentUser = this.token.getUser();

   this.userActive=this.app.userActive;

  }


}
