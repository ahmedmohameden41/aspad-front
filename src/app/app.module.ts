import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { DataTablesModule } from 'angular-datatables';
import { AppRoutingModule } from './app-routing.module';
import { OrganigrammeComponent } from './organigramme/organigramme.component';
import { PlanActionComponent } from './plan-action/plan-action.component';
import { SuiviActiviteComponent } from './suivi-activite/suivi-activite.component';
import { AccueilComponent } from './accueil/accueil.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { LoginComponent } from './referentiel/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './referentiel/register/register.component';
import { ProfileComponent } from './referentiel/profile/profile.component';
import { authInterceptorProviders } from './_helpers/auth.interceptor';
import { ReferentielComponent } from './referentiel/referentiel.component';
import { OrgChartModule } from 'angular-org-chart';
import { AppComponent } from './app.component';
import { AsidenavComponent } from './asidenav/asidenav.component';
import { TableaudebordComponent } from './tableaudebord/tableaudebord.component';
import { EntitesComponent } from './organigramme/entites/entites.component';
import { CollaborateursComponent } from './organigramme/collaborateurs/collaborateurs.component';
import { FonctionsComponent } from './organigramme/fonctions/fonctions.component';
import { InstanceComponent } from './organigramme/instance/instance.component';
import { UtilisateursComponent } from './referentiel/utilisateurs/utilisateurs.component';
import { AxesComponent } from './plan-action/axes/axes.component';
import { ObjectifsComponent } from './plan-action/objectifs/objectifs.component';
import { ActivitesComponent } from './plan-action/activites/activites.component';
import { EquipesComponent } from './suivi-activite/equipes/equipes.component';
import { TachesComponent } from './suivi-activite/taches/taches.component';
import { ActiviteTachesComponent } from './suivi-activite/activite-taches/activite-taches.component';
import { AvatarModule } from 'ngx-avatar';

@NgModule({
  declarations: [
    AppComponent,
    OrganigrammeComponent,
    PlanActionComponent,
    SuiviActiviteComponent,
    AccueilComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    ReferentielComponent,
    AsidenavComponent,
    TableaudebordComponent,
    EntitesComponent,
    CollaborateursComponent,
    FonctionsComponent,
    InstanceComponent,
    UtilisateursComponent,
    AxesComponent,
    ObjectifsComponent,
    ActivitesComponent,
    EquipesComponent,
    TachesComponent,
    ActiviteTachesComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    FormsModule,
    OrgChartModule,
    DataTablesModule,
    NgbModule,
    ReactiveFormsModule,
    AvatarModule
  ],
  providers: [HttpClient,authInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }

export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}
