import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OrgData } from 'angular-org-chart/src/app/modules/org-chart/orgData';
import { AppComponent } from '../app.component';
import { OrganigrammeService } from '../_services/organigramme.service';

@Component({
  selector: 'app-organigramme',
  templateUrl: './organigramme.component.html',
  styleUrls: ['./organigramme.component.css']
})
export class OrganigrammeComponent implements OnInit {

  entites:any;
  idInstance:any= localStorage.getItem('InstanceActive');

  constructor(private organigrammeService:OrganigrammeService, private app:AppComponent, router:Router) {
    this.organigrammeService.getEntites(parseInt(this.idInstance))
    .subscribe(data=>{this.entites=data;},err=>{console.log(err);});
  }

  ngOnInit(): void {}

  orgData: OrgData= {
    name: "DGTIC",
    type: 'LA DIRECTION GENERALE DES TECHNOLOGIES DE L\'INFORMATION ET DE LA COMMUNICATION',
    children: [
        {
            name: "DAE",
            type: 'La Direction de l\'Administration Electronique',
            children: [
                {
                    name: "STI",
                    type: 'Service des Technologies de l\'Internet',
                    children: [
                        {
                            name:"DInter",
                            type:'Division Internet',
                            children:[]
                        },
                        {
                            name:"DIntra",
                            type:'Division Intranet',
                            children:[]
                        },
                        {
                            name:"DInfo",
                            type:'Division Infographie',
                            children:[]
                        }
                    ]
                },
                {
                    name: "SIRI",
                    type: 'Service des Infrastructures Reseaux et Informatiques',
                    children: [
                        {
                            name:"DS",
                            type:'Division des Systeme',
                            children:[]
                        },
                        {
                            name:"DI",
                            type:'Division des Infrastructures',
                            children:[]
                        },
                        {
                            name:"DM",
                            type:'Division de la Maintenance',
                            children:[]
                        }
                    ]
                }, {
                    name: "SSI",
                    type: 'Service de la Securite Informatique',
                    children: [
                        {
                            name:"DSA",
                            type:'Division de la Surveillance et des Alertes',
                            children:[]
                        },
                        {
                            name:"DMOS",
                            type:'Division de la Mise en oeuvre des Outils de Securite',
                            children:[]
                        }
                    ]
                }

            ]
        },
        {
            name: "DSI",
            type: 'La Direction des Systemes d\'Information',
            children: [
                {
                    name: "SED",
                    type: 'Service des Etudes et du Developpement',
                    children: [
                        {
                            name: "DE",
                            type: 'Division des Etudes',
                            children: []
                        }, {
                            name: "DD",
                            type: 'Division du Developpement',
                            children: []
                        }
                    ]
                },
                {
                    name: "SBD",
                    type: 'Service des Bases de Donnees',
                    children: [
                        {
                            name: "DABD",
                            type: 'Division de l\'Administration des Bases de Donnees',
                            children: []
                        },
                        {
                            name: "DE",
                            type: 'Division de l\'Exploitation',
                            children: []
                        }
                    ]
                },
                {
                    name: "SGCAD",
                    type: 'Service de Getion des Contenus et Applications Administratives',
                    children: [
                        {
                            name: "DP",
                            type: 'Division de la Promotion',
                            children: []
                        },
                        {
                            name: "DSE",
                            type: 'Division du Service En ligne',
                            children: []
                        }
                    ]
                }
            ]
        },
        {
            name: "DIPVT",
            type: 'La Direction des Infrastructures, de la Promotion et de la Veille Technologique',
            children: [
                {
                    name: "SRE",
                    type: 'Service des Reseaux et des Equipements',
                    children: [
                        {
                            name: "DP",
                            type: 'Division de la Promotion',
                            children: []
                        }, {
                            name: "DV",
                            type: 'Division de la Vulgarisation',
                            children: []
                        }
                    ]
                },
                {
                    name: "SPV",
                    type: 'Service de la Promotion et de la Vulgarisation',
                    children: [
                        {
                            name: "DP",
                            type: 'Division de la Promotion',
                            children: []
                        }, {
                            name: "DV",
                            type: 'Division de la Vulgarisation',
                            children: []
                        }
                    ]
                },
                {
                    name: "SRVT",
                    type: 'Service de la Recherche et de la Veille Technologique',
                    children: [
                        {
                            name: "DR",
                            type: 'Division de la Recherche',
                            children: []
                        },
                        {
                            name: "DVT",
                            type: 'Division de la Veille Technologique',
                            children: []
                        }
                    ]
                }
            ]
        },
        {
            name: "DRTIC",
            type: 'La Direction de la Reglementation des Technologies de l\'Information et de la Communication',
            children: [
                {
                    name: "SRP",
                    type: 'Service de la Reglementation de la Poste',
                    children: [
                        {
                            name: "DECJ",
                            type: 'Division de l\'Elaboration du Cadre Juridique',
                            children: []
                        }, {
                            name: "DSCCJ avec NNI",
                            type: 'Division du Suivi de la Conformite du Cadre Juridique avec les Normes Nationales et Internationales',
                            children: []
                        }
                    ]
                },
                {
                    name: "SRT",
                    type: 'Service de la Reglementation des Telecommunication',
                    children: [
                        {
                            name: "DECJ",
                            type: 'Division de l\'Elaboration du Cadre Juridique',
                            children: []
                        }, {
                            name: "DSCCJ avec NNI",
                            type: 'Division du Suivi de la Conformite du Cadre Juridique avec les Normes Nationales et Internationales',
                            children: []
                        }
                    ]
                },
                {
                    name: "SRTI",
                    type: 'Service de la Reglementation des Technologie de l\'Information',
                    children: [
                        {
                            name: "DECJ",
                            type: 'Division de l\'Elaboration du Cadre Juridique',
                            children: []
                        }, {
                            name: "DSCCJ avec NNI",
                            type: 'Division du Suivi de la Conformite du Cadre Juridique avec les Normes Nationales et Internationales',
                            children: []
                        }
                    ]
                }
            ]
        },
        {
            name: "DRI",
            type: 'La Direction des Ressources Informatiques',
            children: [
                {
                    name: "SMM",
                    type: 'Service du Materiel et de Maintenance',
                    children: [
                        {
                            name: "DMat",
                            type: 'Division des Materiaux',
                            children: []
                        }, {
                            name: "DMain",
                            type: 'Division de Maintenance',
                            children: []
                        }
                    ]
                },
                {
                    name: "SPS",
                    type: 'Service de Programation et Suivi',
                    children: [
                        {
                            name: "DGSI",
                            type: 'Division de la Getion du Systeme Informatique',
                            children: []
                        },
                        {
                            name: "DS",
                            type: 'Division du Suivi',
                            children: []
                        }
                    ]
                }
            ]
        }
    ]
  };

}
