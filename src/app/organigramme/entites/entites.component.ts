import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { OrganigrammeService } from '../../_services/organigramme.service';
import { NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
@Component({
  selector: 'app-entites',
  templateUrl: './entites.component.html',
  styleUrls: ['./entites.component.css']
})
export class EntitesComponent implements OnInit {

    dtOptions: DataTables.Settings = {};
    entites:any;
    entite:any;
    closeResult: any;
    selectedValue: any;
    idEntiteParent: any;
    idInstance:any= localStorage.getItem('InstanceActive');
    entiteParent = {'idEntite': undefined};

    constructor(private router:Router, private organigrammeService:OrganigrammeService, private app:AppComponent) { }

    ngOnInit(): void {

      this.dtOptions = {
        responsive: false,
        lengthChange: false,
        autoWidth: false,
      };
 
      this.organigrammeService.getEntites(parseInt(this.idInstance))
      .subscribe(data=>{this.entites=data;},err=>{console.log(err);});
    }

    onDeleteEntite(idEntite:number){
      let conf = confirm("Etes vous sure?");
      if(conf) this.organigrammeService.deleteEntite(idEntite).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
    }
    onAddNewEntite(designationEntite:any, abreviationEntite:any, descriptionEntite:any, idEntiteParent:any){
      if(idEntiteParent===undefined || idEntiteParent===""){
        let Entite = {
          "designationEntite" : designationEntite,
          "abreviationEntite" : abreviationEntite,
          "descriptionEntite" : descriptionEntite,
        }
      this.organigrammeService.addNewEntite(Entite, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});
      }else{
        let Entite = {
          "designationEntite" : designationEntite,
          "abreviationEntite" : abreviationEntite,
          "descriptionEntite" : descriptionEntite,
          "entiteParent" : {
            "idEntite" : idEntiteParent
          }
        }
      this.organigrammeService.addNewEntite(Entite, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
      }
     
    }
    onGetEntite(idEntite:number){
      this.entite={};
      this.selectedValue=idEntite;
      this.organigrammeService.getEntite(idEntite).subscribe(data=>{this.entite=data;},err=>{console.log(err);});;
    }
    onSelectedValue(idEntiteParent:any){
      this.idEntiteParent = idEntiteParent;
    }

    onUpdateEntite(designationEntite:any, abreviationEntite:any, descriptionEntite:any, idEntite:number){
      if(this.idEntiteParent===undefined || this.idEntiteParent===""){
        let Entite = {
          "designationEntite" : designationEntite,
          "abreviationEntite" : abreviationEntite,
          "descriptionEntite" : descriptionEntite,
        }
        console.log(Entite);
        this.organigrammeService.updateEntite(Entite, idEntite, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
      }else{
        let Entite = {
          "designationEntite" : designationEntite,
          "abreviationEntite" : abreviationEntite,
          "descriptionEntite" : descriptionEntite,
          "entiteParent" : {
            "idEntite" : this.idEntiteParent
          }
        }
        console.log(Entite);
        this.organigrammeService.updateEntite(Entite, idEntite, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
      }
     
    }

    onEntiteActive(idEntite:any){
      localStorage.setItem('EntiteActive',idEntite);
    }
}
