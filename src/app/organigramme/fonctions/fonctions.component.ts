import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { OrganigrammeService } from '../../_services/organigramme.service';

@Component({
  selector: 'app-fonctions',
  templateUrl: './fonctions.component.html',
  styleUrls: ['./fonctions.component.css']
})
export class FonctionsComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  fonctions: any;
  fonction: any;
  constructor(private router:Router, private organigrammeService:OrganigrammeService, private app:AppComponent) { }


  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.organigrammeService.getFonctions()
      .subscribe(data=>{this.fonctions=data;},err=>{console.log(err);});
  }

  onAddNewFonction(Fonction:Object){
    this.organigrammeService.addNewFonction(Fonction).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeleteFonction(idFonction:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.organigrammeService.deleteFonction(idFonction).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetFonction(idFonction:number){
    this.fonction={};
    this.organigrammeService.getFonction(idFonction).subscribe(data=>{this.fonction=data;},err=>{console.log(err);});;
  }

  onUpdateFonction(Fonction:{}, idFonction:number){
    this.organigrammeService.updateFonction(Fonction, idFonction).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

}
