import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { OrganigrammeService } from '../../_services/organigramme.service';

@Component({
  selector: 'app-collaborateurs',
  templateUrl: './collaborateurs.component.html',
  styleUrls: ['./collaborateurs.component.css']
})

export class CollaborateursComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  collaborateurs:any;
  collaborateur:any;
  selectedRole:any;
  fonctions:any;
  roles = ['gestionnaire','utilisateur','decideur'];
  idEntite:any= localStorage.getItem('EntiteActive');
  idFonction:any;
  collaborateurFonction:any;
  idCollaborateur:any;
  constructor(private router:Router, private organigrammeService:OrganigrammeService, private app:AppComponent) { }

  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.organigrammeService.getCollaborateurs(parseInt(this.idEntite))
      .subscribe(data=>{this.collaborateurs=data;},err=>{console.log(err);});

    this.organigrammeService.getFonctions()
    .subscribe(data=>{this.fonctions=data;},err=>{console.log(err);});
  }

  onAddNewCollaborateur(Collaborateur:Object){
    console.log(Collaborateur);
    this.organigrammeService.addNewCollaborateur(Collaborateur, parseInt(this.idEntite), this.idFonction).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onDeleteCollaborateur(idCollaborateur:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.organigrammeService.deleteCollaborateur(idCollaborateur).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetCollaborateur(idCollaborateur:number){
    this.collaborateur={};
    this.organigrammeService.getCollaborateur(idCollaborateur).subscribe(data=>{this.collaborateur=data;},err=>{console.log(err);});;
  }
  onGetCollaborateurFonction(idCollaborateur:number){
    this.idCollaborateur=idCollaborateur;
    this.collaborateurFonction={};
    this.organigrammeService.getCollaborateurFonction(idCollaborateur).subscribe(data=>{this.collaborateurFonction=data},err=>{console.log(err);});;
  }
  onUpdateCollaborateur(Collaborateur:{}, idCollaborateur:number){
    this.onGetCollaborateurFonction(idCollaborateur);
    this.organigrammeService.updateCollaborateur(Collaborateur, idCollaborateur, parseInt(this.idEntite), this.idFonction).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onAddUser(username:any, email:any, password:any){
    let User = {
      'username':username,
      'email':email,
      'password': password,
      'role': [this.selectedRole]
    }
    this.organigrammeService.addNewUser(User).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }
  
  onSelectedRole(role:any){
    this.selectedRole=role;
  }
  onSelectedFonction(idFonction:any){
    this.idFonction=idFonction;
  }

  onCollaborateurActive(idCollaborateur:any){
    localStorage.setItem('CollaborateurActive', idCollaborateur);
  }

}
