import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { OrganigrammeService } from '../../_services/organigramme.service';

@Component({
  selector: 'app-instance',
  templateUrl: './instance.component.html',
  styleUrls: ['./instance.component.css']
})
export class InstanceComponent implements OnInit {

  dtOptions: DataTables.Settings = {};
  instances: any;
  instance: any;
  constructor(private router:Router, private organigrammeService:OrganigrammeService, private app:AppComponent) { }


  ngOnInit(): void {
    this.dtOptions = {
      responsive: false,
      lengthChange: false,
      autoWidth: false,
    };

    this.organigrammeService.getInstances()
      .subscribe(data=>{this.instances=data;},err=>{console.log(err);});
      this.onGetInstance(1);
  }

  onAddNewInstance(instance:Object){
    console.log(instance);
    this.organigrammeService.addNewInstance(instance).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
    // let Entite = {
    //   "designationEntite" : designationInstance,
    //   "abreviationEntite" : abreviatonInstance,
    //   "descriptionEntite" : descriptionInstance
    // }
    // this.organigrammeService.addNewEntite(Entite, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});
  }

  onDeleteInstance(idInstance:number){
    let conf = confirm("Etes vous sure?");
    if(conf) this.organigrammeService.deleteInstance(idInstance).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

  onGetInstance(idInstance:number){
    this.instance={};
    this.organigrammeService.getInstance(idInstance).subscribe(data=>{this.instance=data;},err=>{console.log(err);});;
  }

  onUpdateInstance(instance:{}, idInstance:number){
    console.log(instance);
    this.organigrammeService.updateInstance(instance, idInstance).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});;
  }

}
