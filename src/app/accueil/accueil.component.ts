import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { OrganigrammeService } from '../_services/organigramme.service';
import { InstanceService } from './services/instance.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.css']
})
export class AccueilComponent implements OnInit {
  instances:any;
  lang:any;
  idInstance:any= localStorage.getItem('InstanceActive');
  direction:any;
  constructor(private organigrammeService:OrganigrammeService, private instanceService:InstanceService, private router:Router, private app:AppComponent) { }

  ngOnInit(): void {
    this.lang = localStorage.getItem('lang') || 'fr';
    if(this.lang==="ar"){
      this.direction ="rtl";
    }else{
      this.direction ="ltr";
    }
    this.instanceService.getInstances()
      .subscribe(data=>{this.instances=data;},err=>{console.log(err);});
  }

  onAddNewInstance(abreviatonInstance:String, designationInstance:String, descriptionInstance:String, telephoneInstance:number, siteWebInstance:String, adresseInstance:String, emailInstance:String, faxInstance:String, bpinstance:String, username:String, password:String){
    let instance = {
      'abreviatonInstance':abreviatonInstance,
      'designationInstance':designationInstance,
      'descriptionInstance':descriptionInstance,
      'telephoneInstance':telephoneInstance,
      'siteWebInstance':siteWebInstance,
      'adresseInstance':adresseInstance,
      'emailInstance':emailInstance,
      'faxInstance':faxInstance,
      'bpinstance':bpinstance
    }    
    this.organigrammeService.addNewInstance(instance).subscribe(data=>{console.log(data);},err=>{console.log(err);});;

    this.onAddUser(username, password, abreviatonInstance);
    // this.onAddEntite(designationInstance, abreviatonInstance, descriptionInstance);
  }

  onAddUser(username:any, password:any, abreviatonInstance:any){
    let User = {
      'username':username,
      'email':'admin'+abreviatonInstance+'@admin',
      'password': password,
      'role': ['admin']
    }
    this.organigrammeService.addNewUser(User).subscribe(data=>{console.log(data);},err=>{console.log(err);});;
  }
  onAddEntite(designationEntite:any, abreviationEntite:any, descriptionEntite:any){
    let Entite = {
      "designationEntite" : designationEntite,
      "abreviationEntite" : abreviationEntite,
      "descriptionEntite" : descriptionEntite
    }
    this.organigrammeService.addNewEntite(Entite, parseInt(this.idInstance)).subscribe(data=>{this.app.reloadCurrentRoute();},err=>{console.log(err);});
  }
  
  onInstanceActive(idInstance:any, AbreviationInstance:any){
    localStorage.setItem('InstanceActive',idInstance);
    localStorage.setItem('AbreviationInstance',AbreviationInstance);
  }
}
