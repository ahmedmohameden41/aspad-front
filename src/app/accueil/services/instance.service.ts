import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

const  URL:String ='http://localhost:8080/';

@Injectable({
  providedIn: 'root'
})

export class InstanceService {

  constructor(private httpClient:HttpClient) { }

  public getInstances(){
    return this.httpClient.get(URL+"instanceses");
  }
}
